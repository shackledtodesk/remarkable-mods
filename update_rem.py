"""
update_rem.py

Update ReMarkable Tablet templates and splashscreens.

Operation:
  1. Backup Templates and Splashscreens
  2. Merge templates.json with custom templates.
  3. Copy updated templates.json, custom templates, and splashscreens to device.
  4. Restart xochitl service to have changes take effect.

TODO: 
  - I'm sure there are bugs in there somewhere :)
  - Enable keybased ssh authentication method
"""

import re
import getpass
import json
import paramiko
import socket
import sys
import os
import datetime
import argparse
from scp import SCPClient
from stat import S_ISDIR, S_ISREG

## Typical USB IP/user of rM device
dest_ip = "10.11.99.1"
dest_user = "root"
dest_rst = "systemctl restart xochitl"


## Where to pull stuff from the rM device
dest_dir = "/usr/share/remarkable"
doc_dir = "/home/root/.local/share/remarkable"

## Where to store backups
bak_dir = "bak"
bak_rmk = "remarkable"
bak_doc = "docs"

splash_dir = ["splashscreens"]
template_dir = ["game-templates", "character-templates", "work-templates"]

####
def open_transport(host, port=22, user="root", pwd="nothing"):
    try:
        transport = paramiko.Transport((host, 22))
    except paramiko.ssh_exception.SSHException as error:
        print()
        print("transport: {}".format(error))
        return(False)
    try:
        transport.connect(username = user, password = pwd)
    except paramiko.ssh_exception.SSHException as error:
        print("connect: {}".format(error))
        return(False)
    return(transport)
    
#########################
## SCP Connect
def scp_connect(transport):
    try:
        sftp = paramiko.SFTPClient.from_transport(transport)
    except paramiko.ssh_exception.SSHException as error:
        print("stfp: {}".format(error))
        return(False)
    return(sftp)


##########################
## Restart over ssh
def ssh_connect(host, port=22, user="root", pwd="nothing", cmd="ls"):
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    except paramiko.ssh_exception.SSHException as error:
        print("connect: {}".format(error))
        return
    try:
        ssh.connect(host, port, username=user, password=pwd, timeout=20, allow_agent=False)
    except paramiko.ssh_exception.SSHException as error:
        print("login: {}".format(error))
        return
    stdin,stdout,stderr=ssh.exec_command(cmd)
    
    
##########################
## SCP Upload
def sftp_put_recursive(path, dest, sftp, ignore=False):
    local_path = path + "/"
    remote_path = dest
    print("Sending: {} -> {}".format(local_path, remote_path))
    sftp.put(local_path, remote_path, recursive=True)

## restore from backup
def restore (user, pwd, host, src_dir, bak_dir):
    port = 22
    transport = open_transport(host, 22, user, pwd)
    if (transport):
        sftp = SCPClient(transport)
        if (sftp):
            sftp_put_recursive(bak_dir, src_dir, sftp)
            sftp.close()
        else:
            return(False)
        transport.close()
        return(True)
    else:
        return(False)

##########################
## Upload customizations
def upload_custom (user, pwd, host, local_dir, dest_dir):
    transport = open_transport(host, 22, user, pwd)
    if transport:
        sftp = SCPClient(transport)
        if sftp:
            if os.path.isdir(local_dir):
                sftp_put_recursive(local_dir, dest_dir, sftp, "transport.json")
            else:
                try:
                    sftp.put(local_dir, dest_dir)
                except paramiko.ssh_exception.SSHException as error:
                    print("sftp put failed: {} -> {} : {}".format(remote_path, local_path, error))
            sftp.close()
        else:
            return(False)
        transport.close()
        return(True)
    else:
        return(False)

##########################
## SCP Backups
def sftp_get_recursive(path, dest, sftp):
    for entry in sftp.listdir_attr(path):
        remote_path = path + "/" + entry.filename
        local_path = os.path.join(dest, entry.filename)
        mode = entry.st_mode

        if S_ISDIR(mode):
            if not args.quiet:
                print(".", end='', flush=True)
            try:
                os.mkdir(local_path)
            except OSError:
                pass
            sftp_get_recursive(remote_path, local_path, sftp)
        elif S_ISREG(mode):
            try:
                sftp.get(remote_path, local_path)
            except paramiko.ssh_exception.SSHException as error:
                print("sftp get failed: {} -> {} : {}".format(remote_path, local_path, error))

def backup (user, pwd, host, src_dir, bak_dir):
    transport = open_transport(host, 22, user, pwd)
    if (transport):
        sftp = scp_connect(transport)
        if (sftp):
            sftp_get_recursive(src_dir, bak_dir, sftp)
            sftp.close()
        else:
            return(False)
        transport.close()
        return(True)
    else:
        return(False)

############################
## Create local backup directories
def init_local(bak_dir, bak_dt, dirs):
    try:
        os.mkdir(bak_dir)
    except OSError:
        pass

    old_date = None
    try:
        os.mkdir("{}/{}".format(bak_dir, dirs))
    except OSError:
        pass
    
    try:
        os.mkdir("{}/{}/{}".format(bak_dir, dirs, bak_dt))
    except OSError:
        pass
    
    try:
        old_date = os.readlink("{}/{}/latest".format(bak_dir, dirs))
    except OSError:
        old_date = None
        pass
    
    ## Symlink from current backup to latests for easier readbility
    try:
        os.remove("{}/{}/latest".format(bak_dir, dirs))
    except OSError as error:
        pass
    
    try:
        os.symlink("{}".format(bak_dt),
                   "{}/{}/latest".format(bak_dir, dirs),
                   target_is_directory=True)
    except OSError as error:
        print("Error: {}".format(error))
        pass

    return(old_date)

def clean_fail(bak_dir, cdate, pdate, dir):
    ## put latest back to last good backup
    ## remove empty backup directories
    try:
        os.remove("{}/{}/latest".format(bak_dir,dir))
    except OSError as error:
        pass

    try:
        os.rmdir("{}/{}/{}".format(bak_dir,dir,cdate))
    except OSError as error:
        print("Failed to delete {}/{}/{}: {}".format(bak_dir,dir,cdate,error))
    
    if pdate is None:
        return
    
    try:
        os.symlink("{}".format(pdate), "{}/{}/latest".format(bak_dir, dir),
                   target_is_directory=True)
    except OSError as error:
        pass
        
##############################
## merge/update template
def update_template(t_src, t_add):
    for itm in t_add['templates']:
        if args.verbose:
            print("       + {}".format(itm['name']))
        for orig in t_src['templates']:
            if orig['name'] == itm['name']:
                orig['filename'] = itm['filename']
                orig['iconCode'] = itm['iconCode']
                orig['categories'] = itm['categories']
                break
        else:
            t_src['templates'].append(itm)

    return(t_src)

## Read in template file
def read_template(t_file):
    try:
        with open(t_file) as file:
            tmpl_s = json.load(file)
    except IOError as error:
        print("Could not read {}: {}".format(src_t, error))
        return None

    return(tmpl_s)

#############################
## parse arguments
def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Maintain customizations and backups of ReMarkable device and documents.",
    )
    parser.add_argument("-d", "--dry-run", action="store_true",
                        help="Just let you know what the actions will be taken.")
    parser.add_argument("-a", "--all", action="store_true", default=False,
                        help="Do all actions (backup, merge templates, add splash screens, restart)")
    talk_group = parser.add_mutually_exclusive_group()
    talk_group.add_argument("-v", "--verbose", action="store_true", help="More verbose output.")
    talk_group.add_argument("-q", "--quiet", action="store_true", help="Run with no output printed.")

    parser.add_argument("-i", "--host", default=dest_ip,
                        help="Hostname or IP of ReMarkable device. %(default)s")
    parser.add_argument("-t", "--templates", action="append", default=template_dir,
                        help="Specify template directories to merge and upload to device. %(default)s")
    parser.add_argument("-s", "--splashscreen", action="append", default=splash_dir,
                        help="Specify splash screen directories to upload to device. %(default)s")

    parser.add_argument("-S", "--splash", action="store_true", help="Update splash screens.")
    parser.add_argument("-T", "--merge-templates", action="store_true", help="Merge and upload templates.")
    parser.add_argument("-R", "--restart", action="store_true", help="Restart ReMarkable App after upload(s).")
    parser.add_argument("-nb", "--no-backup", action="store_true", help="Do not backup ReMarkable first.")
    parser.add_argument("-nt", "--no-templates", action="store_true", help="Do not update templates.")
    parser.add_argument("-ns", "--no-splash", action="store_true", help="Do not update splash screens.")

    act_group = parser.add_mutually_exclusive_group()
    act_group.add_argument("-r", "--restore", nargs='?', default=False, const=True, metavar="datestamp",
                           help="Restore a backup to ReMarkable.  If no date given (YYYYMMDD-HHMMSS), then latest used).")
    
    return(parser)

############################
if __name__ == "__main__":
    parser = init_argparse()
    args = parser.parse_args()

    if args.verbose:
        print(args)

    ## Restoring implies no backup taken first.
    if args.restore:
        args.no_backup = True

    old_date_rmk = None
    old_date_doc = None
    if not args.no_backup:
        ## get time and set backup directory
        when = datetime.datetime.now()
        bak_dt = when.strftime("%Y%m%d-%H%M%S")
        if args.verbose:
            print("Backup directory datestamp: {}".format(bak_dt))
        old_date_rmk = init_local(bak_dir, bak_dt, bak_rmk)
        old_date_doc = init_local(bak_dir, bak_dt, bak_doc)

        bak_rmk_dir = "{}/{}/{}".format(bak_dir, bak_rmk, bak_dt)
        bak_doc_dir = "{}/{}/{}".format(bak_dir, bak_doc, bak_dt)
    else:
        bak_rmk_dir = "{}/{}/latest".format(bak_dir, bak_rmk)
        bak_doc_dir = "{}/{}/latest".format(bak_dir, bak_doc)


    tst_ip = input("Enter ReMarkable IP address [{}]: ".format(dest_ip))
    if len(tst_ip) >= 6:
        dest_ip = tst_ip

    ## get the password
    pwd = getpass.getpass("Password for ReMarkable: ")

    if args.all:
        args.no_backup = False

    ## Restore from backup
    if args.restore:
       if isinstance(args.restore, str) and re.search(r"\d{8}-\d{6}", args.restore):
          bak_rmk_dir = "{}/{}/{}".format(bak_dir, bak_rmk, args.restore)
          bak_doc_dir = "{}/{}/{}".format(bak_dir, bak_doc, args.restore)

       do_rst = input("Restore ReMarkable templates and splashscreens from {}? [y/n]".format(bak_rmk_dir))
       if do_rst == "y":
          restore(dest_user, pwd, dest_ip, dest_dir, bak_rmk_dir)

       do_rst = input("Restore user documents from {}? [y/n]".format(bak_doc_dir))
       if do_rst == "y":
          restore(dest_user, pwd, dest_ip, doc_dir, bak_doc_dir)

    ###################
    ## Backup remarkable directories
    if not args.no_backup:
        print("Performing backup of {} to {}".format(dest_dir, bak_rmk_dir), end=".", flush=True)
        if not args.dry_run:
            if not backup(dest_user, pwd, dest_ip, dest_dir, bak_rmk_dir):
                clean_fail(bak_dir, bak_dt, old_date_rmk, bak_rmk)
            print(".")
    
        ## Backup personal documents
        print("Performing backup of personal content to {}".format(bak_doc_dir), end=".", flush=True)
        if not args.dry_run:
            if not backup(dest_user, pwd, dest_ip, doc_dir, bak_doc_dir):
                clean_fail(bak_dir, bak_dt, old_date_doc, bak_doc)
            print(".")

    ################### TODO: some checking?  Only allow .png files?
    if args.splash or args.all:
        ## Copying Splashscreens to rM
        for splsh in splash_dir:
            if args.verbose:
                print("Uploading custom splash screens from {}".format(splsh))
                
            if not args.dry_run:
                upload_custom (dest_user, pwd, dest_ip, splsh, dest_dir)

    ################### TODO: into function
    ## merge templates
    if args.merge_templates or args.all:
        ## read main template
        if args.verbose:
            print("Reading latest templates.json file.")
        t_file = "{}/{}/latest/templates/templates.json".format(bak_dir, bak_rmk)
        tmpl_m = read_template(t_file)
        if tmpl_m is None:
            quit()

        if args.verbose:
            print("Reading and merging custom templates.")
        for tmpl in template_dir:
            t_file = "{}/templates.json".format(tmpl)
            if args.verbose:
                print("    - {}".format(t_file))
            tmpl_a = read_template(t_file)
            if tmpl_a:
                tmpl_m = update_template(tmpl_m, tmpl_a)
                if not args.dry_run:
                    if args.verbose:
                        print("Uploading custom templates files for {}".format(tmpl))
                    upload_custom (dest_user, pwd, dest_ip, tmpl, "{}/templates".format(dest_dir))
        

        ## Generating template merge
        if args.verbose:
            print("Writing merged template file: {}/templates.json".format(bak_dir))            
        with open("{}/templates.json".format(bak_dir), 'w') as outfile:
                  json.dump(tmpl_m, outfile, indent=4)

        if os.path.getsize("{}/templates.json".format(bak_dir)) <= 0:
            print("Error: Template merge resulted in corrupt file.")
        else:
            ## Uploading merged template
            if args.verbose:
                print("Uploading new template configuration.  Restart required to take affect.")
            upload_custom(dest_user, pwd, dest_ip, "{}/templates.json".format(bak_dir),
                          "{}/templates/templates.json".format(dest_dir))


    ## Restart the app...
    if args.restart or args.all:
        ## Restart ReMarkable App
        if args.verbose:
            print("Restarting ReMarkable App.")
        ssh_connect(dest_ip, 22, dest_user, pwd, dest_rst)
            
