# Role Playing Game Character Templates

Well, just template at this point.  Its `templates.json` file places
it in the **Games** tab.  The image template and json file is in
[`character-templates`](character-templates) directory.

![icons](examples/character-icons.jpg)

-----

## D&D Character Sheet, 5th Edition

![D&D Character Sheet](examples/game-dnd.jpg)
