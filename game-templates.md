# Pencil Game Templates

Two player games to play on your reMarkable device.  If using the
`templates.json` file included, the templates will be placed in a
separate **Games** tab with the following icons.  The template images
and json file are in the [`game-templates`](game-templates) directory.

![icons](examples/game-icons.jpg)

-----

## Dots and Boxes

Three different sized boards are supplied in the **Dots and Boxes**
template.  Two players take turns drawing a line between dots in a
box.  If a play closes a box, then place their initial in the box and
take another turn.  The player with the most boxes wins.

![Dots and Boxes](examples/game-dots-and-boxes.jpg)

-----

## Go

Go-Moku (or just Go) is a two player game where each player takes
turns placing an 'X' or 'O' on the 19 x 19 grid.  First player to
place 5 in a row (horizontally, vertically, or diagnonally) wins.

![Go](examples/game-go.jpg)

-----

## Hangman

Hangman is a guessing game.  One player thinks up a word or phrase and
writes dashes representing the word to guess.  The other player
guesses letters.  Which each correct guess, the letters are in the
appropriate location.  When a guess is wrong, an additional body part
is added to the drawing.  Game ends when either the word is guessed or
the drawing is complete.

![Hangman](examples/game-hangman.jpg)

-----

## Ultimate Tic-Tac-Toe

This various of **Tic-Tac-Toe** has 9 smaller boards arranged within a
larger board.  The first player places their 'X' in any of the small
boards as normal.  This directs the second player to place their 'O'
on the small board relative to the 'X'. For example, if 'X' player on
the top right square of their small board, then 'O' plays on the small
board at the top right of the larger board.  'X' then plays relative
to where the 'O' was played.

When a small board has a winner, then the 'X' or 'O' of the winner is
place over the small board for the large board.  The next player has
the option to then play on any open small board.

Won and filled boards can no be played upon.  If a player is sent to
one of these small boards, they can choose any other open small board.
The winner is the player who gets three in a row on the large board.

![Ultimate Tic-Tac-Toe](examples/game-ultimate-ttt.jpg)