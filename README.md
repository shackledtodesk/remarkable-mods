# reMarkable mods

Templates and splashscreens for [reMarkable](https://remarkable.com/) tablets.

Details on the templates available here:
 * [Meeting Notes](meeting-templates.md)
 * [Pencil Games](game-templates.md)
 * [Character Sheets](character-templates.md)

## Quickstart (OS X/ Linux)

```shell
$ git clone https://gitlab.com/shackledtodesk/remarkable-mods.git
$ cd remarkable-mods
$ python -m venv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
```

The default action is to backup your content and the remarkable
templates locally (e.g. `bak/docs` and `bak/remarkable`).

```shell
usage: update_rem.py [-h] [-d] [-a] [-v | -q] [-i HOST] [-t TEMPLATES]
                     [-s SPLASHSCREEN] [-S] [-T] [-nb] [-nt] [-ns]
                     [-r [datestamp]]

Maintain customizations and backups of ReMarkable device and documents.

optional arguments:
  -h, --help            show this help message and exit
  -d, --dry-run         Just let you know what the actions will be taken.
  -a, --all             Do all actions (backup, merge templates, update
                        ReMarkable.
  -v, --verbose         More verbose output.
  -q, --quiet           Run with no output printed.
  -i HOST, --host HOST  Hostname or IP of ReMarkable device. 10.11.99.1
  -t TEMPLATES, --templates TEMPLATES
                        Specify template directories to merge and upload to
                        device. ['game-templates', 'character-templates']
  -s SPLASHSCREEN, --splashscreen SPLASHSCREEN
                        Specify splash screen directories to upload to device.
                        ['splashscreens']
  -S, --splash          Update splash screens.
  -T, --merge-templates
                        Merge and upload templates.
  -nb, --no-backup      Do not backup ReMarkable first.
  -nt, --no-templates   Do not update templates.
  -ns, --no-splash      Do not update splash screens.
  -r [datestamp], --restore [datestamp]
                        Restore a backup to ReMarkable. If no date given
                        (YYYYMMDD-HHMMSS), then latest used).
```

**Something here about how to get your IP and password.**

Example usage:

```shell
$ python update_rem.py -v -T 
Namespace(all=False, dry_run=False, host='10.11.99.1', merge_templates=True, no_backup=False, no_splash=False, no_templates=False, quiet=False, restore=None, splash=False, splashscreen=['splashscreens'], templates=['game-templates', 'character-templates'], verbose=True)
Backup directory datestamp: 20210204-113541
Enter ReMarkable IP address [10.11.99.1]: 
Password for ReMarkable: 
Performing backup of /usr/share/remarkable to bak/remarkable/20210204-113541..........
Performing backup of personal content to bak/docs/20210204-113541.............................................................
Reading latest templates.json file.
Reading and merging custom templates.
    - game-templates/templates.json
       + Dots and Boxes
       + Go-Moku
       + Hangman
       + Ultimate Tic-Tac-Toe
Uploading custom templates files for game-templates
    - character-templates/templates.json
       + D&D Character Sheet
Uploading custom templates files for character-templates
Writing merged template file: bak/templates.json
Uploading new template configuration.  Restart required to take affect.
```

-----

## Donate

If you found these templates and tools useful, maybe you'd throw a few coins my way:
  * [Amazon Wishlist](https://www.amazon.com/hz/wishlist/ls/2DEDMJHSWXCI5?ref_=wl_share)
  * [PayPal](https://paypal.me/shackledtodesk?locale.x=en_US)