# Meeting Templates


Templates for various meeting types:

  * Interview Phone Screens
  * General Meetings
  * 1:1 Status Meetings
  * 1:1 SBI (Situation, Behavior, Impact)

The template images and json are available in the [`work-templates`](work-templates) directory.

-----

## Interview Phone Screen

Structured template for taking notes during a phone screen.

![phone screen](examples/work-phone-screen.jpg)

-----

## General Meeting

Multiperson meeting template to note agenda, date, attendees, and action items.

![work meeting](examples/work-meeting.jpg)

-----

## 1:1 Status Meeting

Regular status meeting template between manager and an employee.

![1:1 Status](examples/work-status-1n1.jpg)

-----

## 1:1 SBI Meeting

Situation, Behavior, Impact (SBI) meetings are a structure way of
providing constructive feedback to an employee.  It provides clear
communication to address an observed behavior.  The three sections are as follows:

 1. **Situation**: Describe a specific situation where a behaviour was observed.
 2. **Behavior**: Describe the actual, observed behavior being discussed.  Do not opine or judge, but present only facts.
 3. **Impact**: Describe the results of the behavior.  If the behavior is possitive, use words like "proud" or "happy."  If the behavior is negative, provide examples of how to better approach the situation.

[Reference](https://www.ccl.org/articles/leading-effectively-articles/hr-pipeline-a-quick-win-to-improve-your-talent-development-process/)

![SBI 1:1](examples/work-sbi-1n1.jpg)
